trigger BookTrigger on Book__c (before insert) {
   Book__c[] books = Trigger.new;
   BookDiscounts.applyDiscount(books);
}